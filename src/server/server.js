import express from 'express';
import cors from 'cors';
import path from 'path';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpack from 'webpack';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import Loadable from 'react-loadable';
import App from '../clients/src/App';

import webpackConfig from '../../webpack.config';

const app = express();

// View engine setup
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'ejs');

const compiler = webpack(webpackConfig);

app.use(webpackDevMiddleware(compiler));

app.use(webpackHotMiddleware(compiler));

// Middleware
app.use(cors());

app.use(express.static(__dirname + '/public'));

const router = express.Router();

router.get('/', async (req, res) => {
  const context = {};
  const reactComp = renderToString(
    <StaticRouter location={req.url} context={context}>
      <App />
    </StaticRouter>
  );
  res.status(200).render('index', { reactApp: reactComp });
});

//Routes
app.use('/', router);

const port = process.env.PORT || 3000;

Loadable.preloadAll().then(() => {
  app.listen(port, () => {
    console.log(`App is running at localhost:${port}`);
  });
});
