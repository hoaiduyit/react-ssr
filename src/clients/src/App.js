import React from 'react';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Loadable from 'react-loadable';
import commonReducer from './redux/reducers/commonReducer';
import Loading from './components/Loading';

const Layout = Loadable({
  loader: () => import('./pages/Layout'),
  loading: Loading
});

const reducers = combineReducers({
  commonState: commonReducer
});

const store = createStore(reducers, applyMiddleware(thunk));

export default () => {
  return (
    <Provider store={store}>
      <Layout />
    </Provider>
  );
};
