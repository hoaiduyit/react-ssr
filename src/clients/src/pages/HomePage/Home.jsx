import React from 'react';
import Loadable from 'react-loadable';
import Loading from '../../components/Loading';

const Introduce = Loadable({
  loader: () => import('./Sections/Introduce'),
  loading: Loading
});

export default class Home extends React.Component {
  render() {
    return (
      <>
        <Introduce />
      </>
    );
  }
}
