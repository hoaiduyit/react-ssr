import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Loadable from 'react-loadable';
import Loading from '../components/Loading';

const Home = Loadable({
  loader: () => import('./HomePage/Home'),
  loading: Loading
});
const About = Loadable({
  loader: () => import('./About/About'),
  loading: Loading
});
const Header = Loadable({
  loader: () => import('../components/Header'),
  loading: Loading
});
const NavBar = Loadable({
  loader: () => import('../components/NavBar'),
  loading: Loading
});

export default class Layout extends React.Component {
  render() {
    return (
      <>
        <Header />
        <NavBar />
        <Switch>
          <Route exact path="/" render={(props) => <Home {...props} />} />
          <Route exact path="/about" render={(props) => <About {...props} />} />
        </Switch>
      </>
    );
  }
}
