export const COMMON_ACTION = 'COMMON_ACTION';

export const setCommonAction = (payload) => {
  return {
    type: COMMON_ACTION,
    payload
  };
};
