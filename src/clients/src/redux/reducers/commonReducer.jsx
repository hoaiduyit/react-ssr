import { COMMON_ACTION } from '../actions/commonAction';

const initState = {
  commonState: {}
};

export default function commonReducer(state = initState, actions) {
  const { type, payload } = actions;
  switch (type) {
    case COMMON_ACTION:
      return {
        ...state,
        commonState: payload
      };
    default:
      return state;
  }
}
