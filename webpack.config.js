const path = require('path');
const webpack = require('webpack');
const copyWebpackPlugin = require('copy-webpack-plugin');

const config = {
  mode: 'development',
  entry: {
    vendor: ['@babel/polyfill', 'react'],
    index: [
      'webpack-hot-middleware/client?reload=true',
      './src/clients/src/index.js'
    ],
    main: './src/assets/js/main.js'
  },
  output: {
    path: path.resolve(__dirname, 'src', 'public'),
    publicPath: '/',
    filename: '[name].js'
  },
  devServer: {
    overlay: true,
    hot: true,
    host: 'localhost'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        },
        exclude: [/node_modules/, /public/, '/assets/js/']
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader']
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader']
      }
    ]
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new copyWebpackPlugin([{ from: './src/assets/', to: 'assets' }])
  ],
  resolve: {
    extensions: ['.js', '.jsx', '*']
  }
};

module.exports = config;
